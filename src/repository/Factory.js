import ConfigRepository from 'src/repository/ConfigRepository'
import RollRepository from "src/repository/RollRepository";
import UserRepository from "src/repository/UserRepository";

const repositories = {
  config: ConfigRepository,
  roll: RollRepository,
  user: UserRepository
}

export const factory = {
  get: name => repositories[name]
}
