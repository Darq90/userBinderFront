import Repository from 'src/repository/Repository'

const resource = '/roll'
const probabilityDistribution = '/roll/probabilityDistribution'

export default {
  makeRoll(config, games = 1, rolls = 1) {
    let data = {
      config: config,
      rolls: rolls,
      games: games
    };

    return Repository.post(`${resource}`, JSON.stringify(data))
  },
  getProbabilityDistribution(config) {
    let data = {
      config: config
    };

    return Repository.post(`${probabilityDistribution}`, JSON.stringify(data))
  }
}
